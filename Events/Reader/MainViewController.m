//
//  MainViewController.m
//  Events
//
//  Created by Brian Huynh
//  Copyright (c) 2014 Brian Huynh. All rights reserved.

#import "MainViewController.h"
#import "WebViewController.h"
#import <MessageUI/MessageUI.h>
#import "GoogleMapsViewController.h"


@interface MainViewController ()

@end

@implementation NSString (mycategory)

- (NSString *)stringByStrippingHTML 
{
    NSRange r;
    NSString *s = [self copy];
    while ((r = [s rangeOfString:@"<[^>]+>" options:NSRegularExpressionSearch]).location != NSNotFound)
        s = [s stringByReplacingCharactersInRange:r withString:@""];
    return s; 
}

@end

@implementation MainViewController
@synthesize parseResults = _parseResults;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (NSUInteger) supportedInterfaceOrientations {
    
    return UIInterfaceOrientationMaskPortrait;
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    
    // Fix Main View Logo
    
    UIImageView* logo = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"LogoBlue"]];
    self.navigationItem.titleView = logo;
     

    self.navigationController.navigationBar.barTintColor =[UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1.0];
    self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1.0];
    self.navigationController.navigationBar.translucent = YES;
    
    [self setNeedsStatusBarAppearanceUpdate];
    
    _mediaPlaybackAllowsAirPlay = YES;
    
    UIBarButtonItem *campusMap = [[UIBarButtonItem alloc] initWithTitle:@"Map" style:UIBarButtonItemStyleBordered target:self action:@selector(loadCampusMap)];
    
     campusMap.tintColor = [UIColor colorWithRed:0/255.0 green:48/255.0 blue:75/255.0 alpha:1.0];
    
    self.navigationItem.rightBarButtonItem = campusMap;
        self.navigationController.navigationBar.tintColor =  [UIColor colorWithRed:0/255.0 green:48/255.0 blue:75/255.0 alpha:1.0];
    
    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObject:[UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:1.0] forKey:NSForegroundColorAttributeName]];
    
    //Set table row height so it can fit title & 2 lines of summary
    self.tableView.rowHeight = 80;
    
    UIRefreshControl * refresh = [[UIRefreshControl alloc]init];
    refresh.attributedTitle = [NSAttributedString alloc];
    
    [refresh addTarget:self action:@selector(reloadFeed) forControlEvents:UIControlEventValueChanged];
    self.refreshControl = refresh;
    
    [refresh setTintColor: [UIColor colorWithRed:0/255.0 green:48/255.0 blue:75/255.0 alpha:1.0]];
    
    
    NSURL *scriptUrl = [NSURL URLWithString:@"http://apple.com"];
    NSData *data = [NSData dataWithContentsOfURL:scriptUrl];
    
    if (!data) {
        [self noInternet];
    }
    
    //Parse feed
    KMXMLParser *parser = [[KMXMLParser alloc] initWithURL:@"http://www.shacknews.com/rss?recent_articles=1" delegate:self];
    _parseResults = [parser posts];
   
    // [self numberOfEventsToday];
    /*
     // Displays font menu in Console
     
    for (NSString* family in [UIFont familyNames])
    {
        NSLog(@"%@", family);
        
        for (NSString* name in [UIFont fontNamesForFamilyName: family])
        {
            NSLog(@"  %@", name);
        }
    }
   
    UITapGestureRecognizer * doubleTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(showActionSheet)];
    doubleTap.numberOfTapsRequired = 2;
    doubleTap.delegate = self;
    [self.tableView addGestureRecognizer:doubleTap];
    
    
    UILongPressGestureRecognizer *lpgr = [[UILongPressGestureRecognizer alloc]
                                          initWithTarget:self action:@selector(handleLongPress:)];
    lpgr.minimumPressDuration = 0.1; //seconds
    lpgr.delegate = self;
    [self.tableView addGestureRecognizer:lpgr];
    */
    
    
    [self stripHTMLFromSummary];
    
}


- (void)stopRefresh {
    
    [self.refreshControl endRefreshing];
}

- (void) noInternet {
    UIAlertView * internet = [[UIAlertView alloc] initWithTitle:@"" message:@"Internet Access Not Detected. Cannot retrieve Events." delegate:self cancelButtonTitle:@"Dismiss" otherButtonTitles:nil];
    [internet show];
}

// Double Tap to display new menu (Future)
/*
-(void)showActionSheet {
    
UIActionSheet *popup = [[UIActionSheet alloc] initWithTitle:@"Menu:" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles: @"Find Me", nil];
    popup.tag = 1;
    [popup showInView:[UIApplication sharedApplication].keyWindow];
}

- (void)actionSheet:(UIActionSheet *)popup clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    switch (popup.tag) {
        case 1: {
            switch (buttonIndex) {
                case 0:
                    break;
                default:
                    break;
            }
            break;
        }
        default:
            break;
    }
}
*/


-(void) emailButtonTouched {

MFMailComposeViewController *mail = [[MFMailComposeViewController alloc] init];
mail.mailComposeDelegate = self;
[mail setToRecipients:[NSArray arrayWithObject:@"brianh.idea@gmail.com"]];
[mail setSubject:@"Suggestions/Problems"];
[mail setMessageBody:@"" isHTML:NO];
[self presentViewController:mail animated:YES completion:nil];
    
}

- (void)mailComposeController:(MFMailComposeViewController*)controller
          didFinishWithResult:(MFMailComposeResult)result
                        error:(NSError*)error;
{
    if (result == MFMailComposeResultSent) {
        NSLog(@"Message Sent!");
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];

}


-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

- (void)viewDidUnload
{
    [NSNotificationCenter defaultCenter];
    [super viewDidUnload];
    
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)stripHTMLFromSummary {
    
    int i = 0;
    int count = self.parseResults.count;
   
    //cycles through each 'summary' element stripping HTML
    while (i < count) {

       NSString * tempString = [[self.parseResults objectAtIndex:i] objectForKey:@"summary"];
        NSString *strippedString = [tempString stringByStrippingHTML];
        NSMutableDictionary *dict = [self.parseResults objectAtIndex:i];
        [dict setObject:strippedString forKey:@"summary"];
        [self.parseResults replaceObjectAtIndex:i withObject:dict];
        i++;
    }
    
}

- (void)reloadFeed {
    
        KMXMLParser *parser = [[KMXMLParser alloc] initWithURL:@"http://www.shacknews.com/rss?recent_articles=1" delegate:self];
        _parseResults = [parser posts];
        [self stripHTMLFromSummary];
        [self.tableView reloadData];
        [self stopRefresh];
    
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.parseResults.count;
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    
    if (buttonIndex == 1) {

        [self emailButtonTouched];
    }
    
}

-(NSInteger)numberOfEventsToday {
    
    NSInteger todaysEvent  = 0;
    int first_run = 1;
    
    // Runs once to establish baseline.  Afterwards, it checks NSUserDefaults if the app was opened on the same day
    NSDateComponents *today = [[NSCalendar currentCalendar] components:NSEraCalendarUnit|NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit |NSHourCalendarUnit |NSMinuteCalendarUnit fromDate:[NSDate date]];
    
    NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
    
    NSDate * compareDate = [defaults objectForKey:@"today"];
    [defaults setObject:today forKey:@"today"];
    [defaults setInteger:first_run forKey:@"first_run"];
    [defaults synchronize];
    

    
    for (NSUInteger i = 0; i < _parseResults.count; i++)
    {
        NSString * eventDate = [[self.parseResults objectAtIndex:i]objectForKey:@"date"];
        eventDate = [eventDate stringByReplacingOccurrencesOfString:@"\n" withString:@""];
        eventDate = [eventDate stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        NSLocale *enUS = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
        NSDateFormatter *df = [[NSDateFormatter alloc] init];
        
        [df setDateFormat:@"EEE, dd MMM yyyy HH:mm:ss zzz"];
        [df setLocale:enUS];
        df.timeZone = [NSTimeZone systemTimeZone];
        NSDate *date = [df dateFromString:eventDate];
        
            NSDateComponents *event = [[NSCalendar currentCalendar] components:NSEraCalendarUnit|NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit fromDate:date];
        
        NSDateComponents *today = [[NSCalendar currentCalendar] components:NSEraCalendarUnit|NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit |NSHourCalendarUnit |NSMinuteCalendarUnit fromDate:[NSDate date]];
        
        if([today day] == [event day] && [today month] == [event month] && [today year] == [event year] && [today era] == [event era])
        {
            todaysEvent++;
        }
        else
        {
            UILocalNotification *local = [[UILocalNotification alloc]init];
            local.applicationIconBadgeNumber = todaysEvent;
            [[UIApplication sharedApplication]scheduleLocalNotification:local];
            return todaysEvent;
        }
    }
            return 0;
}

-(void)loadCampusMap
{
    GoogleMapsViewController * map = [[GoogleMapsViewController alloc]init];
    [self.navigationController pushViewController:map animated:YES];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView setBackgroundColor:[UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1.0]];
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];

    //Check if cell is nil. If it is create a new instance of it
    if (cell == nil ) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }

        [cell setBackgroundColor:[UIColor whiteColor]];
        cell.textLabel.textColor =  [UIColor colorWithRed:0/255.0 green:48/255.0 blue:75/255.0 alpha:1.0];
        // Configure titleLabel
        cell.textLabel.text = [[self.parseResults objectAtIndex:indexPath.row] objectForKey:@"title"];
        cell.textLabel.numberOfLines = 2;
        cell.textLabel.font = [UIFont fontWithName:@"" size:16];
        //cell.textLabel.font = [UIFont fontWithName:@"GosmickSansOblique" size:16];
        cell.detailTextLabel.text = @"";
    
    NSString *yourString = [NSString stringWithFormat:@"%@", [[self.parseResults objectAtIndex:indexPath.row]objectForKey:@"date"]];
    yourString = [yourString stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    yourString = [yourString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    NSLocale *enUS = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
   
    [df setDateFormat:@"EEE, dd MMM yyyy HH:mm:ss zzz"];
    [df setLocale:enUS];
    df.timeZone = [NSTimeZone systemTimeZone];
    
    NSDate *date = [df dateFromString:yourString];
    //NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];

    NSDateComponents *event = [[NSCalendar currentCalendar] components:NSEraCalendarUnit|NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit fromDate:date];
    
    NSDateComponents *today = [[NSCalendar currentCalendar] components:NSEraCalendarUnit|NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit |NSHourCalendarUnit |NSMinuteCalendarUnit fromDate:[NSDate date]];
    
   
    if([today day] == [event day] && [today month] == [event month] && [today year] == [event year] && [today era] == [event era] && [today hour] >= [event hour] + 2) {
        
        cell.detailTextLabel.text = @"Done";
         cell.detailTextLabel.font = [UIFont fontWithName:@"" size:5];
        //cell.detailTextLabel.font = [UIFont fontWithName:@"Chams-Bold" size:10];
        cell.detailTextLabel.textColor = [UIColor colorWithRed:0/255.0 green:48/255.0 blue:75/255.0 alpha:1.0];
    }
    
    else if([today day] == [event day] && [today month] == [event month] && [today year] == [event year] && [today era] == [event era] && [event hour] >= 17 ) {
        
        cell.detailTextLabel.text = @"Tonight!";
        cell.detailTextLabel.font = [UIFont fontWithName:@"" size:5];
        //cell.detailTextLabel.font = [UIFont fontWithName:@"Chams-Bold" size:10];
        cell.detailTextLabel.textColor = [UIColor colorWithRed:0/255.0 green:48/255.0 blue:75/255.0 alpha:1.0];
        cell.textLabel.textColor = [UIColor colorWithRed:212/255.0 green:175/255.0 blue:55/255.0 alpha:1.0];
    }
    
    else if([today day] == [event day] && [today month] == [event month] && [today year] == [event year] && [today era] == [event era]) {
        
        cell.detailTextLabel.text = @"Today!";
        cell.detailTextLabel.font = [UIFont fontWithName:@"" size:5];
        //cell.detailTextLabel.font = [UIFont fontWithName:@"Chams-Bold" size:10];
        cell.detailTextLabel.textColor = [UIColor colorWithRed:0/255.0 green:48/255.0 blue:75/255.0 alpha:1.0];
        cell.textLabel.textColor = [UIColor colorWithRed:212/255.0 green:175/255.0 blue:55/255.0 alpha:1.0];
    }
     
    return cell;
    
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
   
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    NSString *url = [[self.parseResults objectAtIndex:indexPath.row] objectForKey:@"link"];
    NSString *title = [[self.parseResults objectAtIndex:indexPath.row] objectForKey:@"title"];
    
        WebViewController *vc = [[WebViewController alloc] initWithURL:url title:title];
        [self.navigationController pushViewController:vc animated:YES];
    
}

// Hold to view photo
-(void)handleLongPress:(UILongPressGestureRecognizer *)gestureRecognizer
{
    
    CGPoint p = [gestureRecognizer locationInView:self.tableView];
    
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:p];

  //  UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
    
    // Image View PoC from RSS
    
    /*
    NSString * imageUrl =  [[self.parseResults objectAtIndex:indexPath.row] objectForKey:@"image"];
    UIImageView *imageView = [[UIImageView alloc]init];
     imageView.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:imageUrl]]];
    */
    
 //   UIImage * image = [[UIImage alloc] init];
    
 //    CGFloat width = image.size.width;
 //    CGFloat height = image.size.height;
    
    
    UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Time"]];
    imageView.userInteractionEnabled = YES;
    imageView.tag = 1;
    
    
    /*
    if (width > height) {
        CGAffineTransform rotate = CGAffineTransformMakeRotation( 1.0 / 90.0 * 3.14 );
        [imageView setTransform:rotate];
    }
     */
    
   UIWindow *keyWindow = [[UIApplication sharedApplication] keyWindow];
    
    if (gestureRecognizer.state == UIGestureRecognizerStateBegan) {
         NSLog(@"long press on table view at row %ld", (long)indexPath.row);
        
        [imageView setFrame:keyWindow.bounds];
        [keyWindow addSubview:imageView];
        [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:NO];

    }
    
    if (gestureRecognizer.state == UIGestureRecognizerStateEnded) {
        NSLog(@"Done");
        //[imageView release];
        [[keyWindow viewWithTag:1] removeFromSuperview];
         [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:NO];
    }
}


#pragma mark - KMXMLParser Delegate

- (void)parserDidFailWithError:(NSError *)error {
    
         UIAlertView *noEvents = [[UIAlertView alloc] initWithTitle:@"" message:@"No Events Found." delegate:self cancelButtonTitle:@"Dismiss" otherButtonTitles:@"Contact", nil];
         [noEvents show];

    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
}

- (void)parserCompletedSuccessfully {
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
}

- (void)parserDidBegin {
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
}

@end
