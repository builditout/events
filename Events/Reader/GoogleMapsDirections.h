//
//  GoogleMapsDirections.h
//  Events
//
//  Created by Brian Huynh on 7/20/14.
//  Copyright (c) 2014 HotRod software. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GoogleMapsDirections : NSObject


- (void)setDirectionsQuery:(NSDictionary *)object withSelector:(SEL)selector
              withDelegate:(id)delegate;
- (void)retrieveDirections:(SEL)sel withDelegate:(id)delegate;
- (void)fetchedData:(NSData *)data withSelector:(SEL)selector
       withDelegate:(id)delegate;
    


@end
